Recursion is an elegant technique if used correctly. If used wrongly it can give very poor performance.
multiplying long integer numbers in the format m * n. 
write a function for multiplication.
that performs better and does not exhaust the stack.
 So calculating 5 * 1000 takes 10 steps and 5 * 1000000 takes 20 steps. To do so you should avoid calling the same function twice to calculate the same result.
